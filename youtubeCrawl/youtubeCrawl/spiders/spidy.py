# -*- coding: utf-8 -*-
import scrapy


class SpidySpider(scrapy.Spider):
    name = 'spidy'
    allowed_domains = ['www.youtube.com']
    start_urls = ['http://www.youtube.com/']

    def parse(self, response):
       	f=open("demo.html","w+")
       	for i in response.xpath("//img/@src").extract():
       		f.write(f"<img src='{str(i)}'/>")
